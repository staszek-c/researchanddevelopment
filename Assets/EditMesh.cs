﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EditMesh : MonoBehaviour
{

    Mesh mesh;

    List<int> trianglesList;
    List<int> trimmedList;
    List<int> oryginaltriangleList;
    public int triangleCount;
    public int oryginaltriangleCount;
    float deleteRatio;
    int amountTrisToDelete;
    

    void Awake()
    {
        mesh = GetComponent<MeshFilter>().mesh;
        trianglesList = mesh.triangles.ToList<int>();
        oryginaltriangleList = new List<int>(trianglesList);
        oryginaltriangleCount = (oryginaltriangleList.Count / 3);
        deleteRatio = 0.1f;
    }

    void Start()
    {
        InvokeRepeating("MeshRandomizer", 0.0f, 0.25f);
        InvokeRepeating("ResetMesh", 0.24f, 0.25f);
    }

    void MeshRandomizer()
    {
        Debug.Log("Mesh randomizer started.");
        Debug.Log("Triangle list count to randomize: " + trianglesList.Count);


        triangleCount = (trianglesList.Count / 3);
        amountTrisToDelete = (int)(triangleCount * deleteRatio);
        trimmedList = trianglesList;
        mesh.SetTriangles(trianglesList, 0);
        Debug.Log(mesh.triangles.Length);

        for (int i = 0; i < amountTrisToDelete; i++)
        {
            int triToDelete = Random.Range(1, triangleCount);
                trimmedList.RemoveRange((triToDelete - 1) * 3, 3);
                triangleCount--;

            
        }

        mesh.SetTriangles(trimmedList, 0);

        Debug.Log(mesh.triangles.Length);
        Debug.Log("Mesh has been changed.");
        Debug.Log("---------------------");

    }
    void ResetMesh()
    {
        mesh.SetTriangles(oryginaltriangleList, 0);
        trianglesList = mesh.triangles.ToList<int>();

        
        Debug.Log("Mesh has been reset and how has: " + mesh.triangles.Count() + " triangles.");
        deleteRatio = Random.Range(0.1f, 0.3f);
    }



}