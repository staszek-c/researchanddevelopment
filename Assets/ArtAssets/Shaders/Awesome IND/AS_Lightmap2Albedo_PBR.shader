// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Awesome IND/AS Fake Triplanar Lightmap"
{
	Properties
	{
		_FakeLightmapTex("FakeLightmapTex", 2D) = "white" {}
		_Multiply("Multiply", Vector) = (1,1,1,0)
		_Add("Add", Vector) = (0,0,0,0)
		[HDR]_Color("Color", Color) = (1,1,1,0)
		_FresnelMultiply("FresnelMultiply", Float) = 0
		[Toggle]_ToggleSwitch0("Toggle Switch0", Float) = 0
		[Toggle]_ToggleSwitch1("Toggle Switch1", Float) = 0
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float3 worldPos;
			float3 worldNormal;
		};

		uniform float4 _Color;
		uniform sampler2D _FakeLightmapTex;
		uniform float _ToggleSwitch1;
		uniform float _ToggleSwitch0;
		uniform float3 _Multiply;
		uniform float3 _Add;
		uniform float _FresnelMultiply;

		inline fixed4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return fixed4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float3 ase_worldPos = i.worldPos;
			float temp_output_24_0 = ( ( ase_worldPos.y * _Multiply.y ) + _Add.y );
			float temp_output_26_0 = ( ( ase_worldPos.z * _Multiply.z ) + _Add.z );
			float2 appendResult11 = (float2(temp_output_24_0 , temp_output_26_0));
			float temp_output_43_0 = ( ( ase_worldPos.x * _Multiply.x ) + _Add.x );
			float2 appendResult10 = (float2(temp_output_43_0 , temp_output_26_0));
			float2 appendResult13 = (float2(temp_output_43_0 , temp_output_24_0));
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float3 ase_worldNormal = i.worldNormal;
			float fresnelNDotV50 = dot( normalize( ase_worldNormal ), ase_worldViewDir );
			float fresnelNode50 = ( 0.0 + 1.0 * pow( 1.0 - fresnelNDotV50, 5.0 ) );
			o.Emission = ( ( _Color * tex2D( _FakeLightmapTex, lerp(lerp(appendResult11,appendResult10,_ToggleSwitch0),appendResult13,_ToggleSwitch1) ) ) + ( fresnelNode50 * _FresnelMultiply ) ).rgb;
			o.Alpha = 1;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Unlit keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float3 worldPos : TEXCOORD1;
				float3 worldNormal : TEXCOORD2;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				fixed3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.worldNormal = worldNormal;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				float3 worldPos = IN.worldPos;
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = IN.worldNormal;
				SurfaceOutput o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutput, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14401
0;560;1203;458;1891.475;566.5463;1.616984;True;False
Node;AmplifyShaderEditor.CommentaryNode;47;-1576.943,-17.83296;Float;False;301;151;Z;2;26;23;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;45;-1582.961,-340.5419;Float;False;302.037;146.4178;X;2;44;43;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;46;-1579.364,-184.235;Float;False;300.0377;158.5822;Y;2;22;24;;1,1,1,1;0;0
Node;AmplifyShaderEditor.Vector3Node;42;-1946.943,-185.833;Float;False;Property;_Multiply;Multiply;1;0;Create;True;1,1,1;1,0.38,0.232;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldPosInputsNode;54;-1971.772,-478.141;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;44;-1568.961,-297.5419;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;41;-1945.443,-41.16331;Float;False;Property;_Add;Add;2;0;Create;True;0,0,0;0,-0.146,0.135;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;22;-1562.364,-132.6528;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;23;-1562.009,27.50641;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;24;-1415.326,-134.235;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;26;-1410.542,26.39246;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;43;-1425.924,-299.1241;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;10;-1060.665,-180.8686;Float;False;FLOAT2;4;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;11;-1052.665,-280.8686;Float;False;FLOAT2;4;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;13;-1055.665,-75.86859;Float;False;FLOAT2;4;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ToggleSwitchNode;62;-520.0142,-316.7767;Float;False;Property;_ToggleSwitch0;Toggle Switch0;5;0;Create;True;0;2;0;FLOAT2;0.0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ToggleSwitchNode;63;-419.1345,-106.5796;Float;False;Property;_ToggleSwitch1;Toggle Switch1;6;0;Create;True;0;2;0;FLOAT2;0.0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.FresnelNode;50;-19.08348,-35.87304;Float;False;Tangent;4;0;FLOAT3;0,0,0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;3;FLOAT;5.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;48;-16.51027,-420.6255;Float;False;Property;_Color;Color;3;1;[HDR];Create;True;1,1,1,0;1,1,1,1;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;53;-19.08357,105.6582;Float;False;Property;_FresnelMultiply;FresnelMultiply;4;0;Create;True;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;9;-161.1696,-228.5948;Float;True;Property;_FakeLightmapTex;FakeLightmapTex;0;0;Create;True;None;30cd4e6466221db429fc07533727372c;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;52;244.6792,23.31284;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;49;240.8193,-335.7067;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WeightedBlendNode;60;-756.2324,-51.67881;Float;False;5;0;FLOAT3;0,0;False;1;FLOAT2;0.0;False;2;FLOAT2;0,0;False;3;FLOAT2;0,0;False;4;FLOAT;0.0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.WeightedBlendNode;58;-747.7429,-207.1446;Float;False;5;0;FLOAT3;0,0,0;False;1;FLOAT2;0.0;False;2;FLOAT2;0,0;False;3;FLOAT2;0.0,0;False;4;FLOAT;0.0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.WeightedBlendNode;39;-749.5557,-362.3347;Float;False;5;0;FLOAT3;0,0,0;False;1;FLOAT2;0;False;2;FLOAT2;0,0;False;3;FLOAT2;0,0;False;4;FLOAT;0.0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.AbsOpNode;14;-1222.403,-542.1287;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;31;-1050.637,-545.8434;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.PosVertexDataNode;16;-1974.263,-323.1274;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;51;452.8442,-280.7195;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.NormalVertexDataNode;36;-1445.051,-557.1726;Float;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;610.1716,-312.8443;Float;False;True;2;Float;ASEMaterialInspector;0;0;Unlit;Awesome IND/AS Fake Triplanar Lightmap;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;0;0;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;44;0;54;1
WireConnection;44;1;42;1
WireConnection;22;0;54;2
WireConnection;22;1;42;2
WireConnection;23;0;54;3
WireConnection;23;1;42;3
WireConnection;24;0;22;0
WireConnection;24;1;41;2
WireConnection;26;0;23;0
WireConnection;26;1;41;3
WireConnection;43;0;44;0
WireConnection;43;1;41;1
WireConnection;10;0;43;0
WireConnection;10;1;26;0
WireConnection;11;0;24;0
WireConnection;11;1;26;0
WireConnection;13;0;43;0
WireConnection;13;1;24;0
WireConnection;62;0;11;0
WireConnection;62;1;10;0
WireConnection;63;0;62;0
WireConnection;63;1;13;0
WireConnection;9;1;63;0
WireConnection;52;0;50;0
WireConnection;52;1;53;0
WireConnection;49;0;48;0
WireConnection;49;1;9;0
WireConnection;60;0;31;0
WireConnection;60;1;11;0
WireConnection;60;2;11;0
WireConnection;60;3;11;0
WireConnection;58;0;31;0
WireConnection;58;1;10;0
WireConnection;58;2;13;0
WireConnection;58;3;11;0
WireConnection;39;0;31;0
WireConnection;39;1;11;0
WireConnection;39;2;13;0
WireConnection;39;3;10;0
WireConnection;14;0;36;0
WireConnection;31;0;14;0
WireConnection;31;1;14;0
WireConnection;51;0;49;0
WireConnection;51;1;52;0
WireConnection;0;2;51;0
ASEEND*/
//CHKSM=2B168ADFE8697309843E268AFC7F5FFD2455E8FD