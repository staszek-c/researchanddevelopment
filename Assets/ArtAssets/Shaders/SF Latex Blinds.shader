// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:3,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:True,rprd:True,enco:False,rmgx:True,imps:True,rpth:1,vtps:0,hqsc:False,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:False,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:7964,x:34102,y:32626,varname:node_7964,prsc:2|diff-9150-OUT,spec-3412-OUT,gloss-3412-OUT,emission-4157-OUT,amdfl-6028-OUT,difocc-490-G,clip-6774-OUT;n:type:ShaderForge.SFN_Tex2d,id:490,x:32029,y:33143,varname:_MainTex1,prsc:2,ntxv:0,isnm:False|TEX-9438-TEX;n:type:ShaderForge.SFN_Vector1,id:3412,x:33828,y:32646,varname:node_3412,prsc:0,v1:0;n:type:ShaderForge.SFN_Multiply,id:4157,x:32957,y:33092,varname:node_4157,prsc:2|A-6426-OUT,B-2403-OUT,C-4449-RGB;n:type:ShaderForge.SFN_Multiply,id:4854,x:32392,y:32534,varname:node_4854,prsc:2|A-1654-RGB,B-490-G;n:type:ShaderForge.SFN_Color,id:1654,x:32027,y:32526,ptovrint:False,ptlb:Color,ptin:_Color,varname:_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:4449,x:32025,y:33355,ptovrint:False,ptlb:EmissiveColor,ptin:_EmissiveColor,varname:_EmissiveColor,prsc:2,glob:False,taghide:False,taghdr:True,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0.984712,c3:0.922,c4:1;n:type:ShaderForge.SFN_Tex2d,id:5402,x:32027,y:32298,ptovrint:False,ptlb:LatexTex,ptin:_LatexTex,varname:_LatexTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False|UVIN-3224-OUT;n:type:ShaderForge.SFN_Lerp,id:6321,x:33363,y:32383,varname:node_6321,prsc:2|A-4854-OUT,B-5402-RGB,T-105-OUT;n:type:ShaderForge.SFN_TexCoord,id:9879,x:30457,y:32314,varname:node_9879,prsc:2,uv:1,uaff:False;n:type:ShaderForge.SFN_Slider,id:4994,x:30073,y:33339,ptovrint:False,ptlb:BlindsRoll,ptin:_BlindsRoll,varname:_BlindsRoll,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Add,id:9526,x:32030,y:33628,varname:node_9526,prsc:2|A-4994-OUT,B-930-V;n:type:ShaderForge.SFN_TexCoord,id:930,x:31701,y:33636,varname:node_930,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_RemapRange,id:5028,x:30436,y:33339,varname:node_5028,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1|IN-4994-OUT;n:type:ShaderForge.SFN_Add,id:6050,x:30744,y:32380,varname:node_6050,prsc:2|A-9879-V,B-5641-OUT,C-4994-OUT;n:type:ShaderForge.SFN_Append,id:3224,x:31027,y:32340,varname:node_3224,prsc:2|A-9879-U,B-6050-OUT;n:type:ShaderForge.SFN_Vector1,id:5641,x:30457,y:32464,varname:node_5641,prsc:2,v1:-1;n:type:ShaderForge.SFN_Step,id:6774,x:32243,y:33578,varname:node_6774,prsc:2|A-287-OUT,B-9526-OUT;n:type:ShaderForge.SFN_Vector1,id:287,x:32030,y:33555,varname:node_287,prsc:2,v1:1;n:type:ShaderForge.SFN_Tex2dAsset,id:9438,x:31726,y:33141,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:_MainTex,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:1439,x:32029,y:32956,varname:_node_8988,prsc:2,ntxv:0,isnm:False|UVIN-3224-OUT,TEX-9438-TEX;n:type:ShaderForge.SFN_Multiply,id:9150,x:33828,y:32517,varname:node_9150,prsc:2|A-6321-OUT,B-1439-R;n:type:ShaderForge.SFN_ValueProperty,id:6028,x:33828,y:32769,ptovrint:False,ptlb:DiffuseAmbient,ptin:_DiffuseAmbient,varname:_DiffuseAmbient,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Tex2d,id:7167,x:32029,y:32734,ptovrint:False,ptlb:PatternTex,ptin:_PatternTex,varname:_PatternTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-916-UVOUT;n:type:ShaderForge.SFN_Multiply,id:6426,x:32607,y:32912,varname:node_6426,prsc:2|A-9249-OUT,B-490-B;n:type:ShaderForge.SFN_ValueProperty,id:265,x:31220,y:32626,ptovrint:False,ptlb:PatternTileY,ptin:_PatternTileY,varname:_PatternTileY,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_ComponentMask,id:7561,x:31220,y:32457,varname:node_7561,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-3224-OUT;n:type:ShaderForge.SFN_Multiply,id:5952,x:31416,y:32538,varname:node_5952,prsc:2|A-7561-G,B-265-OUT;n:type:ShaderForge.SFN_ValueProperty,id:8210,x:31220,y:32340,ptovrint:False,ptlb:PatternTileX,ptin:_PatternTileX,varname:_PatternTileX,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:5659,x:31416,y:32408,varname:node_5659,prsc:2|A-8210-OUT,B-7561-R;n:type:ShaderForge.SFN_Append,id:7747,x:31619,y:32469,varname:node_7747,prsc:2|A-5659-OUT,B-5952-OUT;n:type:ShaderForge.SFN_Multiply,id:2403,x:32607,y:33042,varname:node_2403,prsc:2|A-1439-R,B-1439-R,C-1439-R;n:type:ShaderForge.SFN_Clamp01,id:9249,x:32432,y:32890,varname:node_9249,prsc:2|IN-6103-OUT;n:type:ShaderForge.SFN_Slider,id:6360,x:31289,y:33107,ptovrint:False,ptlb:PatternOpacity,ptin:_PatternOpacity,varname:_PatternOpacity,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Add,id:6103,x:32257,y:32890,varname:node_6103,prsc:2|A-7167-R,B-316-OUT;n:type:ShaderForge.SFN_Slider,id:4669,x:31289,y:32966,ptovrint:False,ptlb:PatternLatexMask,ptin:_PatternLatexMask,varname:_PatternLatexMask,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Clamp01,id:2226,x:32607,y:32738,varname:node_2226,prsc:0|IN-6602-OUT;n:type:ShaderForge.SFN_Add,id:6602,x:32432,y:32738,varname:node_6602,prsc:2|A-4636-OUT,B-2896-OUT;n:type:ShaderForge.SFN_OneMinus,id:316,x:31726,y:32977,varname:node_316,prsc:2|IN-6360-OUT;n:type:ShaderForge.SFN_Rotator,id:916,x:31847,y:32573,varname:node_916,prsc:2|UVIN-7747-OUT,PIV-8799-OUT,ANG-9077-OUT;n:type:ShaderForge.SFN_Slider,id:2972,x:30906,y:32769,ptovrint:False,ptlb:PatternAngle,ptin:_PatternAngle,varname:_PatternAngle,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Tau,id:2625,x:31081,y:32891,varname:node_2625,prsc:2;n:type:ShaderForge.SFN_Multiply,id:9077,x:31312,y:32792,varname:node_9077,prsc:2|A-2972-OUT,B-2625-OUT;n:type:ShaderForge.SFN_Vector1,id:5779,x:31416,y:32688,varname:node_5779,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Vector2,id:8799,x:31626,y:32669,varname:node_8799,prsc:2,v1:0.5,v2:0.5;n:type:ShaderForge.SFN_OneMinus,id:4636,x:31726,y:32849,varname:node_4636,prsc:2|IN-4669-OUT;n:type:ShaderForge.SFN_ToggleProperty,id:1809,x:32560,y:32136,ptovrint:False,ptlb:node_1809,ptin:_node_1809,varname:_node_1809,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False;n:type:ShaderForge.SFN_OneMinus,id:2896,x:32257,y:32751,varname:node_2896,prsc:2|IN-7167-R;n:type:ShaderForge.SFN_SwitchProperty,id:105,x:33217,y:32633,ptovrint:False,ptlb:FlipPatternLatexMask,ptin:_FlipPatternLatexMask,varname:_FlipPatternLatexMask,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-3747-OUT,B-3364-OUT;n:type:ShaderForge.SFN_Multiply,id:3747,x:32908,y:32555,varname:node_3747,prsc:2|A-5402-A,B-2226-OUT;n:type:ShaderForge.SFN_OneMinus,id:6868,x:32798,y:32738,varname:node_6868,prsc:2|IN-2226-OUT;n:type:ShaderForge.SFN_Multiply,id:3364,x:32983,y:32719,varname:node_3364,prsc:2|A-5402-A,B-6868-OUT;proporder:1654-4449-5402-4994-9438-6028-7167-265-8210-6360-4669-2972-105;pass:END;sub:END;*/

Shader "Awesome IND/SF Latex Blinds" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        [HDR]_EmissiveColor ("EmissiveColor", Color) = (1,0.984712,0.922,1)
        _LatexTex ("LatexTex", 2D) = "black" {}
        _BlindsRoll ("BlindsRoll", Range(0, 1)) = 0
        _MainTex ("MainTex", 2D) = "white" {}
        _DiffuseAmbient ("DiffuseAmbient", Float ) = 1
        _PatternTex ("PatternTex", 2D) = "white" {}
        _PatternTileY ("PatternTileY", Float ) = 1
        _PatternTileX ("PatternTileX", Float ) = 1
        _PatternOpacity ("PatternOpacity", Range(0, 1)) = 0
        _PatternLatexMask ("PatternLatexMask", Range(0, 1)) = 0
        _PatternAngle ("PatternAngle", Range(0, 1)) = 0
        [MaterialToggle] _FlipPatternLatexMask ("FlipPatternLatexMask", Float ) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        LOD 200
        Pass {
            Name "DEFERRED"
            Tags {
                "LightMode"="Deferred"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_DEFERRED
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile ___ UNITY_HDR_ON
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _Color;
            uniform float4 _EmissiveColor;
            uniform sampler2D _LatexTex; uniform float4 _LatexTex_ST;
            uniform float _BlindsRoll;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _DiffuseAmbient;
            uniform sampler2D _PatternTex; uniform float4 _PatternTex_ST;
            uniform float _PatternTileY;
            uniform float _PatternTileX;
            uniform float _PatternOpacity;
            uniform float _PatternLatexMask;
            uniform float _PatternAngle;
            uniform fixed _FlipPatternLatexMask;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            void frag(
                VertexOutput i,
                out half4 outDiffuse : SV_Target0,
                out half4 outSpecSmoothness : SV_Target1,
                out half4 outNormal : SV_Target2,
                out half4 outEmission : SV_Target3 )
            {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                clip(step(1.0,(_BlindsRoll+i.uv0.g)) - 0.5);
////// Lighting:
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                fixed node_3412 = 0.0;
                float gloss = node_3412;
                float perceptualRoughness = 1.0 - node_3412;
                float roughness = perceptualRoughness * perceptualRoughness;
/////// GI Data:
                UnityLight light; // Dummy light
                light.color = 0;
                light.dir = half3(0,1,0);
                light.ndotl = max(0,dot(normalDirection,light.dir));
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = 1;
                #if UNITY_SPECCUBE_BLENDING || UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMin[0] = unity_SpecCube0_BoxMin;
                    d.boxMin[1] = unity_SpecCube1_BoxMin;
                #endif
                #if UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMax[0] = unity_SpecCube0_BoxMax;
                    d.boxMax[1] = unity_SpecCube1_BoxMax;
                    d.probePosition[0] = unity_SpecCube0_ProbePosition;
                    d.probePosition[1] = unity_SpecCube1_ProbePosition;
                #endif
                d.probeHDR[0] = unity_SpecCube0_HDR;
                d.probeHDR[1] = unity_SpecCube1_HDR;
                Unity_GlossyEnvironmentData ugls_en_data;
                ugls_en_data.roughness = 1.0 - gloss;
                ugls_en_data.reflUVW = viewReflectDirection;
                UnityGI gi = UnityGlobalIllumination(d, 1, normalDirection, ugls_en_data );
////// Specular:
                float3 specularColor = node_3412;
                float specularMonochrome;
                float4 _MainTex1 = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float2 node_3224 = float2(i.uv1.r,(i.uv1.g+(-1.0)+_BlindsRoll));
                float4 _LatexTex_var = tex2D(_LatexTex,TRANSFORM_TEX(node_3224, _LatexTex));
                float node_916_ang = (_PatternAngle*6.28318530718);
                float node_916_spd = 1.0;
                float node_916_cos = cos(node_916_spd*node_916_ang);
                float node_916_sin = sin(node_916_spd*node_916_ang);
                float2 node_916_piv = float2(0.5,0.5);
                float2 node_7561 = node_3224.rg;
                float2 node_916 = (mul(float2((_PatternTileX*node_7561.r),(node_7561.g*_PatternTileY))-node_916_piv,float2x2( node_916_cos, -node_916_sin, node_916_sin, node_916_cos))+node_916_piv);
                float4 _PatternTex_var = tex2D(_PatternTex,TRANSFORM_TEX(node_916, _PatternTex));
                fixed node_2226 = saturate(((1.0 - _PatternLatexMask)+(1.0 - _PatternTex_var.r)));
                float4 _node_8988 = tex2D(_MainTex,TRANSFORM_TEX(node_3224, _MainTex));
                float3 diffuseColor = (lerp((_Color.rgb*_MainTex1.g),_LatexTex_var.rgb,lerp( (_LatexTex_var.a*node_2226), (_LatexTex_var.a*(1.0 - node_2226)), _FlipPatternLatexMask ))*_node_8988.r); // Need this for specular when using metallic
                diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, specularColor, specularColor, specularMonochrome );
                specularMonochrome = 1.0-specularMonochrome;
                float NdotV = max(0.0,dot( normalDirection, viewDirection ));
                half grazingTerm = saturate( gloss + specularMonochrome );
                float3 indirectSpecular = (gi.indirect.specular);
                indirectSpecular *= FresnelLerp (specularColor, grazingTerm, NdotV);
/////// Diffuse:
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += float3(_DiffuseAmbient,_DiffuseAmbient,_DiffuseAmbient); // Diffuse Ambient Light
                indirectDiffuse *= _MainTex1.g; // Diffuse AO
////// Emissive:
                float3 emissive = ((saturate((_PatternTex_var.r+(1.0 - _PatternOpacity)))*_MainTex1.b)*(_node_8988.r*_node_8988.r*_node_8988.r)*_EmissiveColor.rgb);
/// Final Color:
                outDiffuse = half4( diffuseColor, _MainTex1.g );
                outSpecSmoothness = half4( specularColor, gloss );
                outNormal = half4( normalDirection * 0.5 + 0.5, 1 );
                outEmission = half4( ((saturate((_PatternTex_var.r+(1.0 - _PatternOpacity)))*_MainTex1.b)*(_node_8988.r*_node_8988.r*_node_8988.r)*_EmissiveColor.rgb), 1 );
                outEmission.rgb += indirectDiffuse * diffuseColor;
                #ifndef UNITY_HDR_ON
                    outEmission.rgb = exp2(-outEmission.rgb);
                #endif
            }
            ENDCG
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _Color;
            uniform float4 _EmissiveColor;
            uniform sampler2D _LatexTex; uniform float4 _LatexTex_ST;
            uniform float _BlindsRoll;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _DiffuseAmbient;
            uniform sampler2D _PatternTex; uniform float4 _PatternTex_ST;
            uniform float _PatternTileY;
            uniform float _PatternTileX;
            uniform float _PatternOpacity;
            uniform float _PatternLatexMask;
            uniform float _PatternAngle;
            uniform fixed _FlipPatternLatexMask;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                LIGHTING_COORDS(4,5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                clip(step(1.0,(_BlindsRoll+i.uv0.g)) - 0.5);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                fixed node_3412 = 0.0;
                float gloss = node_3412;
                float perceptualRoughness = 1.0 - node_3412;
                float roughness = perceptualRoughness * perceptualRoughness;
                float specPow = exp2( gloss * 10.0 + 1.0 );
/////// GI Data:
                UnityLight light;
                #ifdef LIGHTMAP_OFF
                    light.color = lightColor;
                    light.dir = lightDirection;
                    light.ndotl = LambertTerm (normalDirection, light.dir);
                #else
                    light.color = half3(0.f, 0.f, 0.f);
                    light.ndotl = 0.0f;
                    light.dir = half3(0.f, 0.f, 0.f);
                #endif
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = attenuation;
                #if UNITY_SPECCUBE_BLENDING || UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMin[0] = unity_SpecCube0_BoxMin;
                    d.boxMin[1] = unity_SpecCube1_BoxMin;
                #endif
                #if UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMax[0] = unity_SpecCube0_BoxMax;
                    d.boxMax[1] = unity_SpecCube1_BoxMax;
                    d.probePosition[0] = unity_SpecCube0_ProbePosition;
                    d.probePosition[1] = unity_SpecCube1_ProbePosition;
                #endif
                d.probeHDR[0] = unity_SpecCube0_HDR;
                d.probeHDR[1] = unity_SpecCube1_HDR;
                Unity_GlossyEnvironmentData ugls_en_data;
                ugls_en_data.roughness = 1.0 - gloss;
                ugls_en_data.reflUVW = viewReflectDirection;
                UnityGI gi = UnityGlobalIllumination(d, 1, normalDirection, ugls_en_data );
                lightDirection = gi.light.dir;
                lightColor = gi.light.color;
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float LdotH = saturate(dot(lightDirection, halfDirection));
                float3 specularColor = node_3412;
                float specularMonochrome;
                float4 _MainTex1 = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float2 node_3224 = float2(i.uv1.r,(i.uv1.g+(-1.0)+_BlindsRoll));
                float4 _LatexTex_var = tex2D(_LatexTex,TRANSFORM_TEX(node_3224, _LatexTex));
                float node_916_ang = (_PatternAngle*6.28318530718);
                float node_916_spd = 1.0;
                float node_916_cos = cos(node_916_spd*node_916_ang);
                float node_916_sin = sin(node_916_spd*node_916_ang);
                float2 node_916_piv = float2(0.5,0.5);
                float2 node_7561 = node_3224.rg;
                float2 node_916 = (mul(float2((_PatternTileX*node_7561.r),(node_7561.g*_PatternTileY))-node_916_piv,float2x2( node_916_cos, -node_916_sin, node_916_sin, node_916_cos))+node_916_piv);
                float4 _PatternTex_var = tex2D(_PatternTex,TRANSFORM_TEX(node_916, _PatternTex));
                fixed node_2226 = saturate(((1.0 - _PatternLatexMask)+(1.0 - _PatternTex_var.r)));
                float4 _node_8988 = tex2D(_MainTex,TRANSFORM_TEX(node_3224, _MainTex));
                float3 diffuseColor = (lerp((_Color.rgb*_MainTex1.g),_LatexTex_var.rgb,lerp( (_LatexTex_var.a*node_2226), (_LatexTex_var.a*(1.0 - node_2226)), _FlipPatternLatexMask ))*_node_8988.r); // Need this for specular when using metallic
                diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, specularColor, specularColor, specularMonochrome );
                specularMonochrome = 1.0-specularMonochrome;
                float NdotV = abs(dot( normalDirection, viewDirection ));
                float NdotH = saturate(dot( normalDirection, halfDirection ));
                float VdotH = saturate(dot( viewDirection, halfDirection ));
                float visTerm = SmithJointGGXVisibilityTerm( NdotL, NdotV, roughness );
                float normTerm = GGXTerm(NdotH, roughness);
                float specularPBL = (visTerm*normTerm) * UNITY_PI;
                #ifdef UNITY_COLORSPACE_GAMMA
                    specularPBL = sqrt(max(1e-4h, specularPBL));
                #endif
                specularPBL = max(0, specularPBL * NdotL);
                #if defined(_SPECULARHIGHLIGHTS_OFF)
                    specularPBL = 0.0;
                #endif
                half surfaceReduction;
                #ifdef UNITY_COLORSPACE_GAMMA
                    surfaceReduction = 1.0-0.28*roughness*perceptualRoughness;
                #else
                    surfaceReduction = 1.0/(roughness*roughness + 1.0);
                #endif
                specularPBL *= any(specularColor) ? 1.0 : 0.0;
                float3 directSpecular = attenColor*specularPBL*FresnelTerm(specularColor, LdotH);
                half grazingTerm = saturate( gloss + specularMonochrome );
                float3 indirectSpecular = (gi.indirect.specular);
                indirectSpecular *= FresnelLerp (specularColor, grazingTerm, NdotV);
                indirectSpecular *= surfaceReduction;
                float3 specular = (directSpecular + indirectSpecular);
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float nlPow5 = Pow5(1-NdotL);
                float nvPow5 = Pow5(1-NdotV);
                float3 directDiffuse = ((1 +(fd90 - 1)*nlPow5) * (1 + (fd90 - 1)*nvPow5) * NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                indirectDiffuse += float3(_DiffuseAmbient,_DiffuseAmbient,_DiffuseAmbient); // Diffuse Ambient Light
                indirectDiffuse *= _MainTex1.g; // Diffuse AO
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float3 emissive = ((saturate((_PatternTex_var.r+(1.0 - _PatternOpacity)))*_MainTex1.b)*(_node_8988.r*_node_8988.r*_node_8988.r)*_EmissiveColor.rgb);
/// Final Color:
                float3 finalColor = diffuse + specular + emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _Color;
            uniform float4 _EmissiveColor;
            uniform sampler2D _LatexTex; uniform float4 _LatexTex_ST;
            uniform float _BlindsRoll;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _PatternTex; uniform float4 _PatternTex_ST;
            uniform float _PatternTileY;
            uniform float _PatternTileX;
            uniform float _PatternOpacity;
            uniform float _PatternLatexMask;
            uniform float _PatternAngle;
            uniform fixed _FlipPatternLatexMask;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                LIGHTING_COORDS(4,5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                clip(step(1.0,(_BlindsRoll+i.uv0.g)) - 0.5);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                fixed node_3412 = 0.0;
                float gloss = node_3412;
                float perceptualRoughness = 1.0 - node_3412;
                float roughness = perceptualRoughness * perceptualRoughness;
                float specPow = exp2( gloss * 10.0 + 1.0 );
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float LdotH = saturate(dot(lightDirection, halfDirection));
                float3 specularColor = node_3412;
                float specularMonochrome;
                float4 _MainTex1 = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float2 node_3224 = float2(i.uv1.r,(i.uv1.g+(-1.0)+_BlindsRoll));
                float4 _LatexTex_var = tex2D(_LatexTex,TRANSFORM_TEX(node_3224, _LatexTex));
                float node_916_ang = (_PatternAngle*6.28318530718);
                float node_916_spd = 1.0;
                float node_916_cos = cos(node_916_spd*node_916_ang);
                float node_916_sin = sin(node_916_spd*node_916_ang);
                float2 node_916_piv = float2(0.5,0.5);
                float2 node_7561 = node_3224.rg;
                float2 node_916 = (mul(float2((_PatternTileX*node_7561.r),(node_7561.g*_PatternTileY))-node_916_piv,float2x2( node_916_cos, -node_916_sin, node_916_sin, node_916_cos))+node_916_piv);
                float4 _PatternTex_var = tex2D(_PatternTex,TRANSFORM_TEX(node_916, _PatternTex));
                fixed node_2226 = saturate(((1.0 - _PatternLatexMask)+(1.0 - _PatternTex_var.r)));
                float4 _node_8988 = tex2D(_MainTex,TRANSFORM_TEX(node_3224, _MainTex));
                float3 diffuseColor = (lerp((_Color.rgb*_MainTex1.g),_LatexTex_var.rgb,lerp( (_LatexTex_var.a*node_2226), (_LatexTex_var.a*(1.0 - node_2226)), _FlipPatternLatexMask ))*_node_8988.r); // Need this for specular when using metallic
                diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, specularColor, specularColor, specularMonochrome );
                specularMonochrome = 1.0-specularMonochrome;
                float NdotV = abs(dot( normalDirection, viewDirection ));
                float NdotH = saturate(dot( normalDirection, halfDirection ));
                float VdotH = saturate(dot( viewDirection, halfDirection ));
                float visTerm = SmithJointGGXVisibilityTerm( NdotL, NdotV, roughness );
                float normTerm = GGXTerm(NdotH, roughness);
                float specularPBL = (visTerm*normTerm) * UNITY_PI;
                #ifdef UNITY_COLORSPACE_GAMMA
                    specularPBL = sqrt(max(1e-4h, specularPBL));
                #endif
                specularPBL = max(0, specularPBL * NdotL);
                #if defined(_SPECULARHIGHLIGHTS_OFF)
                    specularPBL = 0.0;
                #endif
                specularPBL *= any(specularColor) ? 1.0 : 0.0;
                float3 directSpecular = attenColor*specularPBL*FresnelTerm(specularColor, LdotH);
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float nlPow5 = Pow5(1-NdotL);
                float nvPow5 = Pow5(1-NdotV);
                float3 directDiffuse = ((1 +(fd90 - 1)*nlPow5) * (1 + (fd90 - 1)*nvPow5) * NdotL) * attenColor;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Back
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float _BlindsRoll;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                clip(step(1.0,(_BlindsRoll+i.uv0.g)) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
